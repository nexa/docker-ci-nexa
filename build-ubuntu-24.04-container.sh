set -e
docker build --no-cache -f Ubuntu-Dockerfile-24.04 -t bchunlimited/nexa:ubuntu24.04 .
docker push bchunlimited/nexa:ubuntu24.04
