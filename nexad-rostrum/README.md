# Nexa node + Rostrum server Container image running on Alpine Linux

This Container image [(bchunlimited/nexad-rostrum)](https://hub.docker.com/r/bchunlimited/nexad-rostrum) is based on the minimal [Alpine Linux](https://alpinelinux.org/) with [Nexad v2.0.0.0](https://nexa.org/) node and [Rostrum v11.0.0](https://nexa.gitlab.io/rostrum/) electrum server.


----


## Table of Contents

- [What is Alpine Linux?](#what-is-alpine-linux)
- [Features](#features)
- [Architectures](#architectures)
- [Tags](#tags)
- [How to use this image](#how-to-use-this-image)
- [Source Repository](#source-repositories)
- [Container Registry](#container-registries)
- [Links](#links)


## 🏔️ What is Alpine Linux?
Alpine Linux is a Linux distribution built around musl libc and BusyBox. The image is only 5 MB in size and has access to a package repository that is much more complete than other BusyBox based images. This makes Alpine Linux a great image base for utilities and even production applications. Read more about Alpine Linux here and you can see how their mantra fits in right at home with Container images.

## ✨ Features

* Minimal size only, minimal layers
* Out-of-the-box Nexa node the Rostrum server


## 🏗️ Architectures

* ```:amd64```, ```:x86_64``` - 64 bit Intel/AMD (x86_64/amd64)
* ```:arm64v8```, ```:aarch64``` - 64 bit ARM (ARMv8/aarch64)

#### 📝 PLEASE CHECK TAGS BELOW FOR SUPPORTED ARCHITECTURES, THE ABOVE IS A LIST OF EXPLANATION

## 🏷️ Tags

* ```:latest``` latest stable version based
* ```:dev``` master branch of both Nexa and Rostrum
* ```:version-dev``` latest stable version of Nexa with master branch of Rostrum e.g ```:2.0.0.0-dev```
* ```:version-version``` Version tags in form of `nexa_ver-rostrum_ver` e.g ```:2.0.0.0-11.0.0``` usually inline with latest


## 🚀 How to use this image

### Volume structure

* `/nexa/data`: Blockchain and Rostrum data files


### Configuration file location
The configuration file is located;

* `/nexa/nexa.conf`: Nexad configuration file
* `/etc/rostrum/config.toml`: Rostrum configuration file

If you mount conf files as volume, make sure the files exist on host before starting the container.


### Environment Variables:

* `UID`: specify the user id (default: `1000`)
* `GID`: specify the user group id (default: `1000`)
* `RPC_USER`: specify the User of the RPC connection
* `RPC_PASSWORD`: specify the Password for the RPC connection
* `RPC_PASSWORD_FILE`: path to file include the Password for the RPC connection

It would be best to inject the RPC_PASSWORD_FILE as secrets instead of RPC_PASSWORD


### Ports mapping - Mainnet (default)

* `7227`: Nexa RPC port
* `7228`: Nexa P2P port
* `20001`: Rostrum TCP port
* `20003`: Rostrum WS port

### Ports mapping - Testnet

* `7229`: Nexa RPC port
* `7230`: Nexa P2P port
* `30001`: Rostrum TCP port
* `30003`: Rostrum WS port


### Docker Compose example:

##### (Please pass your own credentials or let them be generated automatically, don't use these ones for production!!)

```yaml
services:

  nexad-rostrum:
    image: bchunlimited/nexad-rostrum
    ports:
      - "7227:7227"
      - "7228:7228"
      - "20001:20001"
      - "20003:20003"
    environment:
      RPC_USER: nexauser
      RPC_PASSWORD: some1234
    volumes:
      - ./data:/nexa/data
      # - ./nexa.conf:/nexa/nexa.conf
      # - ./rostrum.toml:/etc/rostrum/config.toml
    restart: always
    stop_grace_period: 45s
```

IMPORTANT: Both `nexad` and `rostrum` have graceful shutdown processes that may take longer than the default 10-second Docker stop timeout. Therefore, `stop_grace_period` should be configured in `docker-compose.yml` to allow enough time for a complete shutdown before Docker forcefully stops the containers.


## 📚 Source Repository

* [GitLab - bchunlimited/nexad-rostrum](https://gitlab.com/nexa/docker-nexa/-/tree/master/nexad-rostrum)


## 🐳 Container Registry

* [Docker Hub - bchunlimited/nexad-rostrum](https://hub.docker.com/r/bchunlimited/nexad-rostrum/)


## 🔗 Links

* [Nexa Blockchain](https://nexa.org/)

* [GitLab - Nexa](https://gitlab.com/nexa/nexa)

* [GitLab - Rostrum](https://gitlab.com/nexa/rostrum)
