#!/bin/sh

UID=${UID:=1000}
GID=${GID:=1000}

if ! getent group nexuser > /dev/null 2>&1; then
    addgroup -g $GID -S nexuser
fi

if ! getent passwd nexuser > /dev/null 2>&1; then
    adduser -u $UID -D -S -G nexuser nexuser
fi

if [ -f "${RPC_PASSWORD_FILE}" ]; then
    export RPC_PASSWORD=$(cat ${RPC_PASSWORD_FILE})
fi

RPC_USER=${RPC_USER:="user"}
RPC_PASSWORD=${RPC_PASSWORD:="1234"}

chown -R nexuser:nexuser /nexa

exec su-exec nexuser /nexa/bin/nexad -conf="/nexa/nexa.conf" -datadir="/nexa/data" -rpcuser="${RPC_USER}" -rpcpassword="${RPC_PASSWORD}" $@
