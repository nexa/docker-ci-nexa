set -e
docker build --no-cache -f Debian-Dockerfile -t bchunlimited/gitlabci:debian11 .
docker push bchunlimited/gitlabci:debian11
